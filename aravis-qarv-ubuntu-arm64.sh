#!/bin/bash
apt update && apt full-upgrade -y
apt install -y libgtk2.0-dev libxml2-dev libusb-1.0-0-dev libcap-ng-dev libaudit-dev libnotify-dev libgtk-3-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgirepository1.0-dev gstreamer1.0-plugins-bad intltool gstreamer1.0-plugins-good libcanberra-gtk3-module libqt4-dev libopencv-dev libavcodec-dev cmake doxygen graphviz git gtk-doc-tools
mkdir ~/sources && cd ~/sources && git clone https://github.com/AravisProject/aravis.git && cd aravis
./autogen.sh --enable-usb --enable-packet-socket --enable-viewer --enable-gst-plugin --enable-fast-heartbeat --enable-introspection=yes --enable-gtk-doc-pdf
ARCH=arm64 make -j$(($(nproc)+1))
ARCH=arm64 make install
ldconfig
cd ~/sources && git clone https://github.com/AD-Vega/qarv.git
cd qarv && mkdir build && cd build
cmake ..
make -j$(($(nproc)+1))
make install

#!/bin/bash
apt update && apt full-upgrade -y
apt install autoconf intltool autogen gcc pkg-config libglib2.0-dev libxml2-dev cmake libqt4-dev libswscale-dev libopencv-dev libavcodec-dev
mkdir ~/sources && cd ~/sources && git clone https://github.com/AravisProject/aravis.git && cd aravis
./configure --enable-usb=no --enable-packet-socket --enable-viewer=no --enable-gst-plugin=no --enable-fast-heartbeat --disable-gtk-doc
make
make install
ldconfig
cd ~/sources && git clone https://github.com/AD-Vega/qarv.git
cd qarv && mkdir build && cd build
cmake ..
make
make install

#! /bin/bash
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
NC="\033[0m"



clear
if [ ! -d macchiatobin ]; then
	mkdir macchiatobin
	echo -e "${GREEN}Created macchiatobin folder in this directory${NC}"
	export BASEDIR=$(pwd)/macchiatobin
	echo -e "${BLUE}Project will be build in ${NC}"$BASEDIR/"${BLUE} except for OpenWRT which is in /opt${NC}"
 elif [ -d macchiatobin ]; then
	export BASEDIR=$(pwd)/macchiatobin
	echo -e "${BLUE}Project is being build in ${NC}"$BASEDIR/"${BLUE} except for OpenWRT which is in /opt${NC}"
fi

function press_enter
{
	echo -e ""
	echo -e -n "${BLUE}Press Enter to continue${NC}"
	read
	clear
}

function install_toolchain

{
	clear
	echo -e "${BLUE}Installing dependencies${NC}"
	sudo apt install -y ubuntu-dev-tools sed make binutils build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python unzip rsync libncurses5*
	cd $BASEDIR
	echo -e "${GREEN}Dependencies installed${NC}"
	
	if [ -d gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu ]; then
		echo -e "${BLUE}Toolchain folder already exists${NC}"
	elif [ ! -d gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu ]; then
		if [ -f gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz ]; then
			echo -e "${BLUE}Toolchain archive already downloaded${NC}"
			echo -e "${BLUE}Extracting toolchain archive${NC}"
			tar -xvf gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz
			echo -e "${GREEN}Extraction completed${NC}"
			rm gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz
			echo -e "${BLUE}Deleting downloaded archive${NC}"
		elif [ ! -f gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz ]; then 
			echo -e "${BLUE}Downloading toolchain${NC}"
			wget https://releases.linaro.org/components/toolchain/binaries/5.5-2017.10/aarch64-linux-gnu/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz
			echo -e "${BLUE}Extracting toolchain archive${NC}"
			tar -xvf gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz
			echo -e "${GREEN}Extraction completed${NC}"
			rm gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu.tar.xz
			echo -e "${BLUE}Deleting downloaded archive${NC}"
		fi
	
	fi		


	export CROSS_COMPILE=$BASEDIR/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	echo -e "${GREEN}Exported CROSS_COMPILE path: ${NC}$BASEDIR/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-"
	echo -e "${GREEN}Done${NC}"
}

function build_uboot

{
	clear
	echo -e "${BLUE}Installing dependencies${NC}"
	sudo apt install -y device-tree-compiler swig libpython-dev libssl-dev gcc-aarch64-linux-gnu
	echo -e "${GREEN}Dependencies installed${NC}"
	cd $BASEDIR
	export CROSS_COMPILE=$BASEDIR/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	if [ ! -d u-boot-marvell ]; then
		echo -e "${BLUE}Cloning Marvell U-Boot repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/u-boot-marvell -b u-boot-2017.03-armada-17.10
		cd u-boot-marvell/
		echo -e "${BLUE}Downloading and applying patch${NC}"
		wget http://wiki.macchiatobin.net/tiki-download_file.php?fileId=50
		git apply 0001-mcbin-u-boot-and-env-on-SD-card-mmc-1.patch
	fi
	cd u-boot-marvell/
	echo -e "${BLUE}Default configuration${NC}"
	make mvebu_mcbin-88f8040_defconfig
	echo -e "${GREEN}Done${NC}"
	echo -e "${BLUE}Building U-Boot${NC}"
	make
	echo -e "${GREEN}Done${NC}"
	export BL33=$BASEDIR/u-boot-marvell/u-boot.bin
	echo -e "${GREEN}Exported U-Boot path: ${NC}$BASEDIR/u-boot-marvell/u-boot.bin"
	cd $BASEDIR
	if [ ! -d binaries-marvell ]; then
		echo -e "${BLUE}Cloning Marvell binaries repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/binaries-marvell -b binaries-marvell-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	export SCP_BL2=$BASEDIR/binaries-marvell/mrvl_scp_bl2_8040.img
	echo -e "${GREEN}Exported SCP_BL2 path: ${NC}${BASEDIR}/binaries-marvell/mrvl_scp_bl2_8040.img"
	if [ ! -d atf-marvell ]; then
		echo -e "${BLUE}Cloning Marvell ATF repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/atf-marvell.git -b atf-v1.3-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	if [ ! -d mv-ddr-marvell ]; then
		echo -e "${BLUE}Cloning Marvell DDR repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git -b mv_ddr-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	cd $BASEDIR/atf-marvell/
	echo -e "${BLUE}Building ATF${NC}"
	make USE_COHERENT_MEM=0 LOG_LEVEL=20 MV_DDR_PATH=${BASEDIR}/mv-ddr-marvell PLAT=a80x0_mcbin all fip
	echo -e "${GREEN}Done${NC}"
}

function build_uefi

{
	clear
	echo -e "${BLUE}Installing dependencies${NC}"
	sudo apt install -y device-tree-compiler swig libpython-dev libssl-dev gcc-aarch64-linux-gnu
	echo -e "${GREEN}Dependencies installed${NC}"
	cd ${BASEDIR}
	export GCC5_AARCH64_PREFIX=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	if [ ! -d edk2 ]; then
		echo -e "${BLUE}Cloning EDK II repository${NC}"
		git clone https://github.com/tianocore/edk2
		echo -e "${GREEN}Done${NC}"
	fi
	if [ ! -d edk2-open-platform ]; then
		echo -e "${BLUE}Cloning EDK II Marvell repository, Armada Work in Progress branch${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/edk2-open-platform.git -b marvell-armada-wip
		echo -e "${GREEN}Done${NC}"
	fi
	cd ${BASEDIR}/edk2
	ln -s ${BASEDIR}/edk2-open-platform OpenPlatformPkg
	echo -e "${GREEN}Created symbolic link ${NC}${BASEDIR}/edk2-open-platform${GREEN} in ${NC}${BASEDIR}/edk2"
	echo -e "${BLUE}Building UEFI${NC}"
	make -C BaseTools
	source edksetup.sh
	build -a AARCH64 -t GCC5 -b RELEASE -p OpenPlatformPkg/Platforms/Marvell/Armada/Armada80x0McBin.dsc
	echo -e "${GREEN}Building UEFI done${NC}"
	export BL33=${BASEDIR}/edk2/Build/Armada80x0McBin-AARCH64/RELEASE_GCC5/FV/ARMADA_EFI.fd
	echo -e "${GREEN}Exported UEFI path: ${NC}${BASEDIR}/edk2/Build/Armada80x0McBin-AARCH64/RELEASE_GCC5/FV/ARMADA_EFI.fd"
	cd ${BASEDIR}
	if [ ! -d atf-marvell ]; then
		echo -e "${BLUE}Cloning Marvell ATF repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/atf-marvell.git -b atf-v1.3-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	if [ ! -d mv-ddr-marvell ]; then
		echo -e "${BLUE}Cloning Marvell DDR repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git -b mv_ddr-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	if [ ! -d binaries-marvell ]; then
		echo -e "${BLUE}Cloning Marvell binaries repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/binaries-marvell.git -b binaries-marvell-armada-17.10
		echo -e "${GREEN}Done${NC}"
	fi
	export SCP_BL2=${BASEDIR}/binaries-marvell/mrvl_scp_bl2_8040.img
	echo -e "${GREEN}Exporteds SCP_BL2 path: ${NC}${BASEDIR}/binaries-marvell/mrvl_scp_bl2_8040.img"
	export ARCH=arm64
	export CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	cd ${BASEDIR}/atf-marvell/
	echo -e "${BLUE}Building ATF${NC}"
	make USE_COHERENT_MEM=0 LOG_LEVEL=20 MV_DDR_PATH=${BASEDIR}/mv-ddr-marvell PLAT=a80x0_mcbin all fip
	echo -e "${GREEN}Done${NC}"
}


function build_kernel_armada

{
	clear
	cd ${BASEDIR}
	if [ ! -d linux-marvell ]; then	
		echo -e "${BLUE}Cloning Marvell Linux kernel repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/linux-marvell.git -b linux-4.4.52-armada-17.10
		echo -e "${GREEN}Done${NC}"
		cd ${BASEDIR}/linux-marvell
		echo -e "${BLUE}Downloading kernel patch${NC}"
		wget https://raw.githubusercontent.com/multiflexi/EIT-Master_Thesis_project/master/patch-linux-headers
		echo -e "${BLUE}Applying kernel patch${NC}"
		patch -p1 -i patch-linux-headers
		echo -e "${GREEN}Done${NC}"
	fi
	export ARCH=arm64
	export CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	echo -e "${BLUE}Default kernel configuration${NC}"
	make mvebu_v8_lsp_defconfig
	echo -e "${BLUE}Building kernel with ${NC}"$(($(nproc)+1))"${BLUE} processes${NC}"
	make -j$(($(nproc)+1))
	echo -e "${GREEN}Done${NC}"
}


function build_buildroot

{
	cd ${BASEDIR}
	if [ ! -d buildroot-marvell ]; then	
		echo -e "${BLUE}Cloning Marvell Buildroot repository${NC}"
		git clone https://github.com/MarvellEmbeddedProcessors/buildroot-marvell.git -b buildroot-2015.11-16.08
		echo -e "${GREEN}Done${NC}"
		cd ${BASEDIR}/buildroot-marvell
		echo -e "${BLUE}Default configuration${NC}"
		make mvebu_armv8_le_defconfig
		echo -e "${BLUE}Downloading fio2.17 patch${NC}"
		wget https://raw.githubusercontent.com/multiflexi/EIT-Master_Thesis_project/master/patch-buildroot
		echo -e "${BLUE}Applying fio2.17 patch${NC}"
		patch -p1 -i patch-buildroot
		echo -e "${GREEN}Done${NC}"
		echo -e "${BLUE}Now you can use this function to enter menuconfig${NC}"
		read -p "Press enter to continue"
	fi
	cd ${BASEDIR}/buildroot-marvell
	make menuconfig
	
	
}


function build_openwrt

{
	clear
	echo -e "${BLUE}Installing dependencies${NC}"
	sudo apt install -y subversion build-essential libncurses5-dev zlib1g-dev gawk git ccache gettext libssl-dev xsltproc file
	echo -e "${GREEN}Dependencies installed${NC}"
	
	if [ -d /opt/kernel ]; then
		echo -e "${BLUE}Kernel folder already exists${NC}"
		cd /opt/kernel
   	elif [ ! -d /opt/kernel ]; then  
		echo -e "${BLUE}Creating kernel directory${NC}"
		sudo mkdir -p /opt/kernel/ && cd /opt/kernel/
	fi
	
	echo -e "${BLUE}Cloning kernel repository, branch 17.10${NC}"
	sudo git clone https://github.com/MarvellEmbeddedProcessors/openwrt-kernel.git -b openwrt_17.10_release

	if [ -d /opt/openwrt ]; then
		echo -e "${BLUE}OpenWRT folder already exists${NC}"
		cd /opt/openwrt
   	elif [ ! -d /opt/openwrt ]; then  
		echo -e "${BLUE}Creating OpenWRT directory${NC}"
		sudo mkdir -p /opt/openwrt/ && cd /opt/openwrt/
	fi

	echo -e "${BLUE}Cloning OpenWRT repository, branch 17.10${NC}"
	sudo git clone https://github.com/MarvellEmbeddedProcessors/openwrt-dd.git -b openwrt_17.10_release
	
	echo -e "${BLUE}Changing owner of both folders to current user${NC}"
	sudo chown -R $(whoami):$(id -gn) /opt/kernel/
	sudo chown -R $(whoami):$(id -gn) /opt/openwrt/openwrt-dd/
	
	echo -e "${BLUE}Updating package feeds${NC}"
	./openwrt-dd/scripts/feeds update -a

	echo -e "${BLUE}Installing all packages${NC}"
	./openwrt-dd/scripts/feeds install -a

	echo -e "${GREEN}Now you can enter menuconfig by typing 'make menuconfig'${NC}"

}

function create_ubuntu16

{	
	cd ${BASEDIR} 
	if [ ! -d ubuntu16 ]; then
		mkdir ubuntu16
		cd ubuntu16
		echo -e "${BLUE}Created ubuntu directory${NC}"
	else 
		cd ubuntu16
	fi
	if [ ! -d temp ]; then	
		mkdir temp
		echo -e "${BLUE}Created temp directory in ubuntu directory${NC}"
	fi
	if [ ! -f ubuntu-16.04.4-server-arm64.iso ]; then
		echo -e "${BLUE}Downloading Ubuntu server 16.04.4 ARM64 image${NC}"	 
		wget http://cdimage.ubuntu.com/releases/16.04/release/ubuntu-16.04.4-server-arm64.iso
		echo -e "${GREEN}Done${NC}"
	fi
	sudo mount -o loop ubuntu-16.04.4-server-arm64.iso temp/
	echo -e "${BLUE}Ubuntu image mounted in temp directory${NC}"	 
	sudo unsquashfs -d rootfs/ temp/install/filesystem.squashfs
	echo -e "${BLUE}Ubuntu fs was unsquashed${NC}"	 
	sudo sed '1s/x//g' rootfs/etc/passwd
	echo -e "${GREEN}Root password removed${NC}"
	echo -e "${BLUE}Copying kernel image and dtb file${NC}"	 
	sudo cp ${BASEDIR}/linux-marvell/arch/arm64/boot/Image rootfs/boot/
	sudo cp ${BASEDIR}/linux-marvell/arch/arm64/boot/dts/marvell/armada-8040-mcbin.dtb rootfs/boot/
	echo -e "${GREEN}Done${NC}"
	echo -e "${BLUE}Creating fs archive${NC}"	 
	sudo tar -cjvf rootfs.tar.bz2 -C rootfs/ .
	sudo chown $LOGNAME rootfs.tar.bz2
	echo -e "${GREEN}Done${NC}" 
	sudo umount temp/
	sudo rm -rf temp
	echo -e "${GREEN}temp directory unmounted and deleted${NC}" 
	echo -e "${BLUE}What is the path to the second partition of your SD card? (like /dev/sdb2, /dev/mmcblk0p2... )${NC}"
	read DEVICEPATH
	sudo mount $DEVICEPATH /mnt/
	cd /mnt/
	sudo tar -xvf ${BASEDIR}/ubuntu16/rootfs.tar.bz2
	echo -e "${BLUE}Now waiting for the data to be really written to the card${NC}"
	sync
	cd 
	sudo umount /mnt
	echo -e "${GREEN}Done, now you can remove your SD card.${NC}"





}

function create_ubuntu18

{	
	cd ${BASEDIR} 
	if [ ! -d ubuntu18 ]; then
		mkdir ubuntu18
		cd ubuntu18
		echo -e "${BLUE}Created ubuntu directory${NC}"
	else 
		cd ubuntu18
	fi
	if [ ! -d temp ]; then	
		mkdir temp
		echo -e "${BLUE}Created temp directory in ubuntu directory${NC}"
	fi
	if [ ! -f ubuntu-18.04-server-arm64.iso ]; then
		echo -e "${BLUE}Downloading Ubuntu server 16.04.4 ARM64 image${NC}"	 
		wget http://cdimage.ubuntu.com/releases/18.04/release/ubuntu-18.04-server-arm64.iso
		echo -e "${GREEN}Done${NC}"
	fi
	sudo mount -o loop ubuntu-18.04-server-arm64.iso temp/
	echo -e "${BLUE}Ubuntu image mounted in temp directory${NC}"	 
	sudo unsquashfs -d rootfs/ temp/install/filesystem.squashfs
	echo -e "${BLUE}Ubuntu fs was unsquashed${NC}"	 
	sudo sed '1s/x//g' rootfs/etc/passwd
	echo -e "${GREEN}Root password removed${NC}"
	echo -e "${BLUE}Copying kernel image and dtb file${NC}"	 
	sudo cp ${BASEDIR}/linux-marvell/arch/arm64/boot/Image rootfs/boot/
	sudo cp ${BASEDIR}/linux-marvell/arch/arm64/boot/dts/marvell/armada-8040-mcbin.dtb rootfs/boot/
	echo -e "${GREEN}Done${NC}"
	echo -e "${BLUE}Creating fs archive${NC}"	 
	sudo tar -cjvf rootfs.tar.bz2 -C rootfs/ .
	sudo chown $LOGNAME rootfs.tar.bz2
	echo -e "${GREEN}Done${NC}" 
	sudo umount temp/
	sudo rm -rf temp
	echo -e "${GREEN}temp directory unmounted and deleted${NC}" 
	echo -e "${BLUE}What is the path to the second partition of your SD card? (like /dev/sdb2, /dev/mmcblk0p2... )${NC}"
	read DEVICEPATH
	sudo mount $DEVICEPATH /mnt/
	cd /mnt/
	sudo tar -xvf ${BASEDIR}/ubuntu18/rootfs.tar.bz2
	echo -e "${BLUE}Now waiting for the data to be really written to the card${NC}"
	sync
	cd 
	sudo umount /mnt
	echo -e "${GREEN}Done, now you can remove your SD card.${NC}"





}


function build_kernel_mainline

{	
	cd ${BASEDIR}
	echo -e "${BLUE}What kernel version do you want to download (4.16.7, 4.14.2, etc.)${NC}"
	read KERNEL_VERSION
	if [ -f linux-$KERNEL_VERSION.tar.xz ]; then
		echo -e "${BLUE}Compressed tarball found, exctracting${NC}"		
		tar -xvf linux-$KERNEL_VERSION.tar.xz
		rm linux-$KERNEL_VERSION.tar.xz

	elif [ ! -d linux-$KERNEL_VERSION ]; then	
		echo -e "${BLUE}Downloading the compressed tarball with kernel version ${NC}$KERNEL_VERSION"
		wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$KERNEL_VERSION.tar.xz
		echo -e "${BLUE}Exctracting${NC}"
		tar -xvf linux-$KERNEL_VERSION.tar.xz
		rm linux-$KERNEL_VERSION.tar.xz

	fi
	cd linux-$KERNEL_VERSION
	echo -e "${BLUE}Default kernel configuration${NC}"
	make ARCH=arm64 CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu- defconfig
	echo -e "${BLUE}Building kernel ${NC}"$KERNEL_VERSION"${BLUE} with ${NC}"$(($(nproc)+1))"${BLUE} processes${NC}"
	make -j$(($(nproc)+1)) ARCH=arm64 CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	echo -e "${GREEN}Done, mainline kernel version ${NC}"$KERNEL_VERSION"${BLUE} compiled${NC}"
}

function build_kernel_ebu

{	
	cd ${BASEDIR}
	echo -e "${BLUE}What branch do you want to download${NC}"
	read BRANCH_NAME
	if [ ! -d mainline-public ]; then	
		echo -e "${BLUE}Cloning EBU Linux kernel repository branch ${NC}$BRANCH_NAME"
		git clone https://github.com/MISL-EBU-System-SW/mainline-public.git -b $BRANCH_NAME
	fi
	cd mainline-public
	echo -e "${BLUE}Default kernel configuration${NC}"
	make ARCH=arm64 CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu- defconfig
	echo -e "${BLUE}Building kernel with ${NC}"$(($(nproc)+1))"${BLUE} processes${NC}"
	make -j$(($(nproc)+1)) ARCH=arm64 CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
	echo -e "${GREEN}Done, EBU kernel compiled${NC}"
}

function clean

{
	sudo rm -rf ${BASEDIR}

}

selection=

until [ "$selection" = "0" ]; do
	echo -e "${YELLOW}
  __  __               _____    _____   _    _   _____              _______    ____    _       _         
 |  \/  |     /\      / ____|  / ____| | |  | | |_   _|     /\     |__   __|  / __ \  | |     (_)        
 | \  / |    /  \    | |      | |      | |__| |   | |      /  \       | |    | |  | | | |__    _   _ __  
 | |\/| |   / /\ \   | |      | |      |  __  |   | |     / /\ \      | |    | |  | | | '_ \  | | | '_ \ 
 | |  | |  / ____ \  | |____  | |____  | |  | |  _| |_   / ____ \     | |    | |__| | | |_) | | | | | | |
 |_|  |_| /_/    \_\  \_____|  \_____| |_|  |_| |_____| /_/    \_\    |_|     \____/  |_.__/  |_| |_| |_|
                                                                                                         
                                                                                                         
${NC}"
	echo -e "Select what to do"
	echo -e "-----DEV TOOLS & ENVIROMENT-----"
	echo -e "1 - Install toolchain"
	echo -e "2 - Get Buildroot/Enter menuconfig" 
	echo -e "-----BOOTLOADERS-----"
	echo -e "10 - Build U-Boot"
	echo -e "11 - Build UEFI EDK II"
	echo -e "-----KERNELS-----"
	echo -e "20 - Build Marvell Armada Linux kernel"
	echo -e "21 - Build mainline kernel"
	echo -e "22 - Build EBU kernel"
	echo -e "-----OpenWRT-----"
	echo -e "30 - Build OpenWRT"
	echo -e "-----Ubuntu-----"
	echo -e "40 - Create Ubuntu 16.04 SD card"
	echo -e "41 - Create Ubuntu 18.04 SD card"
	echo -e "-----Other-----"
	echo -e "99 - Clean everything"
	echo -e "0 - Exit"
	echo -e "" 
	echo -e -n "Enter selection: "
	read selection
	case $selection in
		1 ) install_toolchain; press_enter ;;
		2 ) build_buildroot; press_enter;;

		10 ) build_uboot; press_enter;;
		11 ) build_uefi; press_enter;;

		20 ) build_kernel_armada; press_enter;;
		21 ) build_kernel_mainline; press_enter;;
		22 ) build_kernel_ebu; press_enter;;

		30 ) build_openwrt; press_enter;;

		40 ) create_ubuntu16; press_enter;;
		41 ) create_ubuntu18; press_enter;;	

		99 ) clean; press_enter;;
		0 ) exit;;
		* ) echo -e "Selection not valid"; press_enter;
	esac
done

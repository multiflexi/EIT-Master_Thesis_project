#!/bin/bash
#sudo apt install -y php7.2-cli php7.2-xml
#git clone https://github.com/phoronix-test-suite/phoronix-test-suite.git
#cd phoronix-test-suite
MONITOR=all ./phoronix-test-suite run pts/memory pts/compress-7zip pts/compress-gzip pts/compress-pbzip2 pts/rbenchmark pts/numpy pts/gnupg pts/openssl pts/gcrypt pts/tscp pts/sunflow pts/java-scimark2 pts/mafft pts/graphics-magick pts/c-ray pts/smallpt pts/scimark2 pts/apache pts/nginx pts/encode-mp3 pts/encode-wavpack pts/encode-flac pts/ebizzy pts/stockfish pts/aobench pts/perl-benchmark pts/redis pts/pybench pts/git


MONITOR=all ./phoronix-test-suite run pts/postmark pts/himeno pts/npb

MONITOR=all ./phoronix-test-suite run pts/video-encoding

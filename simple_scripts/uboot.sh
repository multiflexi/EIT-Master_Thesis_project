#!/bin/bash
sudo apt install -y device-tree-compiler swig libpython-dev libssl-dev gcc-aarch64-linux-gnu
cd ~/
mkdir u-boot && cd u-boot
export CROSS_COMPILE=~/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
git clone https://github.com/MarvellEmbeddedProcessors/u-boot-marvell -b u-boot-2017.03-armada-17.10
cd u-boot-marvell/
wget http://wiki.macchiatobin.net/tiki-download_file.php?fileId=50
git apply 0001-mcbin-u-boot-and-env-on-SD-card-mmc-1.patch
make mvebu_mcbin-88f8040_defconfig
make
cd ~/u-boot
export BL33=$(pwd)/u-boot-marvell/u-boot.bin
git clone https://github.com/MarvellEmbeddedProcessors/binaries-marvell -b binaries-marvell-armada-17.10
export SCP_BL2=$(pwd)/binaries-marvell/mrvl_scp_bl2_8040.img
git clone https://github.com/MarvellEmbeddedProcessors/atf-marvell.git -b atf-v1.3-armada-17.10
git clone https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git -b mv_ddr-armada-17.10
cd atf-marvell/
make USE_COHERENT_MEM=0 LOG_LEVEL=20 MV_DDR_PATH=~/u-boot/mv-ddr-marvell PLAT=a80x0_mcbin all fip

#!/bin/bash
sudo apt update && apt full-upgrade -y
sudo apt install -y libgtk2.0-dev libxml2-dev libusb-1.0-0-dev libcap-ng-dev libaudit-dev libnotify-dev libgtk-3-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgirepository1.0-dev gstreamer1.0-plugins-bad intltool gstreamer1.0-plugins-good libcanberra-gtk3-module
cd ~/ && https://github.com/AravisProject/aravis.git && cd aravis
autogen.sh --enable-usb --enable-packet-socket --enable-viewer --enable-gst-plugin --enable-fast-heartbeat --enable-introspection=yes --enable-gtk-doc-pdf
make -j$(($(nproc)+1))
sudo make install
sudo ldconfig


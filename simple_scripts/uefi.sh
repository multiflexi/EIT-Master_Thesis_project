#!/bin/bash
sudo apt install -y device-tree-compiler swig libpython-dev libssl-dev gcc-aarch64-linux-gnu
export BASEDIR=/home/$USER && cd ${BASEDIR}
export CROSS_COMPILE=${BASEDIR}/gcc-linaro-5.5.0-2017.10-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
git clone https://github.com/tianocore/edk2
git clone https://github.com/MarvellEmbeddedProcessors/edk2-open-platform.git -b marvell-armada-wip
cd ${BASEDIR}/edk2
ln -s ${BASEDIR}/edk2-open-platform OpenPlatformPkg
make -C BaseTools
source edksetup.sh
build -a AARCH64 -t GCC5 -b RELEASE -p OpenPlatformPkg/Platforms/Marvell/Armada/Armada80x0McBin.dsc
export BL33=${BASEDIR}/edk2/Build/Armada80x0McBin-AARCH64/RELEASE_GCC5/FV/ARMADA_EFI.fd
# Now  building ATF
cd ${BASEDIR}
git clone https://github.com/MarvellEmbeddedProcessors/atf-marvell.git -b atf-v1.3-armada-17.10
git clone https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git -b mv_ddr-armada-17.10
git clone https://github.com/MarvellEmbeddedProcessors/binaries-marvell.git -b binaries-marvell-armada-17.10
export SCP_BL2=${BASEDIR}/binaries-marvell/mrvl_scp_bl2_8040.img
export ARCH=arm64
cd ${BASEDIR}/atf-marvell/
make USE_COHERENT_MEM=0 LOG_LEVEL=20 MV_DDR_PATH=${BASEDIR}/mv-ddr-marvell PLAT=a80x0_mcbin all fips

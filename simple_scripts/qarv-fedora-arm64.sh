#!/bin/bash
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf check-update && sudo dnf upgrade -y
sudo dnf install -y qt-devel opencv-devel ffmpeg-devel cmake doxygen graphviz
cd ~/ && git clone https://github.com/AD-Vega/qarv.git
cd qarv && mkdir build && cd build
cmake ..
make

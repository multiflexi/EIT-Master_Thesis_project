#!/bin/bash
sudo dnf check-update && sudo dnf upgrade -y
sudo dnf install -y gtk2-devel libxml2-devel libusb-devel libcap-ng-devel audit-libs-devel libnotify-devel gtk3-devel gstreamer1-devel gstreamer1-plugins-base-devel gobject-introspection-devel gstreamer1-plugins-bad-free intltool gtk-doc redhat-rpm-config
cd ~/ && wget http://ftp.acc.umu.se/pub/GNOME/sources/aravis/0.5/aravis-0.5.12.tar.xz && tar xf aravis-0.5.12.tar.xz && cd aravis-0.5.12
./configure --enable-usb --enable-packet-socket --enable-viewer --enable-gst-plugin --enable-fast-heartbeat --enable-introspection=yes --enable-gtk-doc-pdf
ARCH=arm64 make
sudo ARCH=arm64 make install
sudo ldconfig


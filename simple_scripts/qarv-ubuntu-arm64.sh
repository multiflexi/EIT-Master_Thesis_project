#!/bin/bash
apt install -y libqt4-dev libopencv-dev libavcodec-dev cmake doxygen graphviz git
cd ~/ && git clone https://github.com/AD-Vega/qarv.git
cd qarv && mkdir build && cd build
cmake ..
make -j$(($(nproc)+1))
